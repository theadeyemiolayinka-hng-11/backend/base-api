<?php

namespace App\Http\Middleware;

use App\Enums\TaskEnum;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use function App\Helpers\isRequestMethodAccepted;

class TaskCheckerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $task_id = $request->route()->parameter('task_id', null);

        if(is_null($task_id)){
            abort(Response::HTTP_NOT_FOUND, 'Task not found');
        }

        if(!is_string($task_id)){
            abort(Response::HTTP_BAD_REQUEST, 'Task ID must be a string');
        }

        if(in_array($task_id, TaskEnum::values())){
            if (isRequestMethodAccepted($task_id, $request->method())) {
                return $next($request);
            }else{
                abort(Response::HTTP_METHOD_NOT_ALLOWED, 'Method not allowed');
            }
        }else{
            abort(Response::HTTP_NOT_FOUND, 'Task not found');
        }
    }
}
