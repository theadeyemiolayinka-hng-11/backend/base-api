<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiscController extends Controller
{
    public function logs (Request $request) {
        $logs = file_get_contents(storage_path('logs/laravel.log'));
        return response()->json([
            'success' => true,
            'data' => $logs
        ]);
    }

    public function clear_logs (Request $request) {
        file_put_contents(storage_path('logs/laravel.log'), '');
        return response()->json([
            'success' => true,
            'message' => 'Logs cleared successfully'
        ]);
    }

    public function keep_alive (Request $request) {
        return response()->json([
            'success' => true,
            'timestamp' => now(),
            'message' => 'I am alive, TheAdeyemiOlayinka.'
        ]);
    }
}
