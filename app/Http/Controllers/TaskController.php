<?php

namespace App\Http\Controllers;

use App\Helpers\WeatherAPIHelper;
use App\Http\Requests\BaseFormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Throwable;

class TaskController extends Controller
{


    /**
     * Task Matcher
     * @param \App\Http\Requests\BaseFormRequest $request
     * @param mixed $task_id
     * @return JsonResponse
     */
    public function main(BaseFormRequest $request, $task_id): JsonResponse
    {
        try {
            return $this->{'task_' . $task_id}($request);
        } catch (Throwable $e) {
            return response()->json([
                'success' => false,
                'message' => 'An error was encountered.',
                'possible_causes' => [
                    'Task does not exist',
                    'Internal Server Error',
                    'You do not have permission to the resource'
                ]
            ], 404);
        }
    }

    // Task One Handler
    function task_one(BaseFormRequest $request)
    {
        // Get the IP address of the requester, considering possible multiple addresses in X-Forwarded-For
        $clientIp = $request->header('X-Forwarded-For', $request->ip()); // The fallback IP would just plainly be the request IP
        $clientIp = explode(',', $clientIp)[0]; // Take the first IP address in the list

        $weatherAPIHelper = new WeatherAPIHelper();

        $weatherData = $weatherAPIHelper->getIPWeatherInfo($clientIp);

        try{
            // Extract city from weather data
            $city = $weatherData['location']['region'] ?? 'Unknown';
        }catch(Throwable $th){
            $city = 'Unknown';
        }

        try{
            // Extract temperature from weather data
            $temperature = $weatherData['current']['temp_c'] ?? 'Unknown';
        }catch(Throwable $th){
            $temperature = 'Unknown';
        }

        try{
            // Extract visitor_name from query parameters
            $visitorName = trim(trim($request->query('visitor_name', ''), "'"), "\"");
        }catch(Throwable $th){
            $visitorName = '';
        }

        // Format the greeting message
        $greeting = "Hello, {$visitorName}!, the temperature is {$temperature} degrees Celsius in {$city}";

        // Return the response as JSON
        return response()->json([
            'client_ip' => $clientIp,
            'location' => $city,
            'greeting' => $greeting
        ]);
    }
}
