<?php

namespace App\Helpers;


// App-wide Constants

const slackUsername = 'TheAdeyemiOlayinka';
const slackEmail = 'olayinkaapps@gmail.com';
const userName = 'Olayinka Adeyemi';
const userAge = 17;
const userBio = 'I am Olayinka, I am a Full Stack Engineer.';


/**
 * Confirm Request Method Accepted
 * @param string $task
 * @param string $method
 * @return bool
 */
function isRequestMethodAccepted(String $task, String $method): bool
{
    $acceptedMethods = config('task_methods');

    // Check if the task and method combination is accepted
    return isset($acceptedMethods[$task]) && in_array($method, $acceptedMethods[$task]);
}
