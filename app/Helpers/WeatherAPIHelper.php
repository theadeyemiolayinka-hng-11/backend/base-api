<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class WeatherAPIHelper
{
    protected $apiKey;

    protected $endpoint;

    public function __construct(string $apiKey = null, string $endpoint = null)
    {
        $this->apiKey = $apiKey ?? env('WEATHER_API_KEY');
        $this->endpoint = $endpoint ?? env('WEATHER_API_ENDPOINT');
    }

    /**
     * Get the weather information for a ip address
     * @param string $ip
     * @return array
     */
    public function getIPWeatherInfo(string $ip): array
    {
        $weatherResponse = Http::get($this->endpoint, [
            'q' => $ip,
            'key' => $this->apiKey,
            'api' => 'no'
        ]);

        return $weatherResponse->json();
    }
}
