<?php

namespace App\Enums;

use ReflectionClass;

abstract class BaseEnum
{
    /**
     * Store existing constants in a static cache per object.
     *
     *
     * @var array
     */
    protected static $cache = [];

    /**
     * Get an array of constants in class. Constant name in key, constant value in value.
     *
     * @return array
     */
    public static function pairs()
    {
        $class = static::class;

        if (!isset(static::$cache[$class])) {
            static::$cache[$class] = (new ReflectionClass($class))->getConstants();
        }

        return static::$cache[$class];
    }


    /**
     * Get array containing keys of constants in this class.
     *
     * @return array
     */
    public static function keys()
    {
        return \array_keys(static::pairs());
    }

    /**
     * Get array containing keys to be excepted from maps
     *
     * @return array
     */
    public static function excepted()
    {
        return [];
    }

    /**
     * Get array containing keys of constants in this class.
     *
     * @return array
     */
    public static function keysWithoutExcepted()
    {
        return array_diff(static::keys(), static::excepted());
    }

    /**
     * Get array containing values of constants in this class.
     *
     * @return array
     */
    public static function values(): array
    {
        return \array_values(static::pairs());
    }

    /**
     * Check if value is a valid key.
     */
    public static function isValidKey($value, $strict = \false)
    {
        return \in_array($value, static::keys(), $strict);
    }

    /**
     * Check if value is a valid value.
     */
    public static function isValidValue($value, $strict = \false)
    {
        return \in_array($value, static::values(), $strict);
    }

    /**
     * Check if value is a valid key or value.
     *
     * @param mixed $valueOrKey
     * @return boolean
     */
    public static function isValid($valueOrKey)
    {
        return static::isValidKey($valueOrKey) || static::isValidValue($valueOrKey);
    }

    /**
     * Get Key of a Value
     *
     * @param mixed $value
     */
    public static function getKey($value)
    {
        if(static::isValidValue($value)){
            return static::keys()[array_search($value, static::values())];
        }else{
            return null;
        }
    }

    /**
     * Get Key of a Value
     *
     * @param mixed $value
     */
    public static function getName($value)
    {
        $key = static::getKey($value);
        if($key === null){
            return null;
        }else{
            return str_replace('_', ' ', ucwords(strtolower($key)));
        }
    }

    /**
     * Get Value of a Key
     *
     * @param mixed $value
     */
    public static function getValue($key)
    {
        if(static::isValidKey($key)){
            return static::values()[array_search($key, static::keys())];
        }else{
            return null;
        }
    }

    /**
     * Returns a map of the enum keys and their corresponding values.
     *
     * @return array The map of enum keys and values.
     */
    public static function getMap()
    {
        return array_reduce(static::keysWithoutExcepted(), function ($carry, $key) {
            $carry[$key] = static::getValue($key);
            return $carry;
        }, []);
    }

    /**
     * Returns a map of the enum keys and their corresponding values.
     *
     * @return array The map of enum keys and values.
     */
    public static function getNameMap()
    {
        return array_reduce(static::keysWithoutExcepted(), function ($carry, $key) {
            $carry[static::getName(static::getValue($key))] = static::getValue($key);
            return $carry;
        }, []);
    }

    /**
     * Returns a reversed map of the enum keys and their corresponding values.
     *
     * @return array The map of enum keys and values.
     */
    public static function getReversedMap()
    {
        return array_reduce(static::keysWithoutExcepted(), function ($carry, $key) {
            $carry[static::getValue($key)] = $key;
            return $carry;
        }, []);
    }

    /**
     * Returns a reversed map of the enum keys and their corresponding values.
     *
     * @return array The map of enum keys and values.
     */
    public static function getReversedNameMap()
    {
        return array_reduce(static::keysWithoutExcepted(), function ($carry, $key) {
            $carry[static::getValue(static::getValue($key))] = static::getName($key);
            return $carry;
        }, []);
    }
}
