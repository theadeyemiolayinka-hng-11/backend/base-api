<?php

use App\Enums\TaskEnum;

return [

    /*
    |--------------------------------------------------------------------------
    | Accepted Task Request Methods Configuration
    |--------------------------------------------------------------------------
    |
    |
    */

    TaskEnum::TASK_ONE => [
        'GET',
    ]

];
