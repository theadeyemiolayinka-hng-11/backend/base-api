<?php

use App\Http\Controllers\MiscController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::prefix('v1')->group(function () {
    Route::middleware('task')->name('tasks.')->prefix('tasks')->group(function () {
        Route::get('/{task_id}', [TaskController::class, 'main'])->name('task_get');
        Route::post('/{task_id}', [TaskController::class, 'main'])->name('task_post');
    });
    Route::middleware('throttle:5,1')->name('misc.')->prefix('misc')->group(function () {
        Route::get('/logs', [MiscController::class, 'logs'])->name('logs');
        Route::get('/clear-logs', [MiscController::class, 'clear_logs'])->name('clear_logs');
        Route::get('/keep-alive', [MiscController::class, 'keep_alive'])->name('keep_alive');
    });
});
